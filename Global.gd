extends Node

const FISH_SPAWN_INFO = {
	"spungle": 10,
	"mouther": 10,
	"lems": 10,
	"hearthra": 10,
	"carl": 10,
	"cinail": 10,
	"frid": 10,
	"jackospungo": 5,
	"melch": 10,
	"nanba": 10,
	"redacted": 5,
	"smox": 10,
	"treffi": 10,
	"tryndr": 10,
	"ultraSpungle": 5
}

const FISH_NAME_TO_PRELOAD = {
	"spungle": preload("res://Packed/Fish/Spungle.tscn"),
	"mouther": preload("res://Packed/Fish/Mouther.tscn"),
	"lems": preload("res://Packed/Fish/Lems.tscn"),
	"hearthra": preload("res://Packed/Fish/Hearthra.tscn"),
	"carl": preload("res://Packed/Fish/Carl.tscn"),
	"cinail": preload("res://Packed/Fish/Cinail.tscn"),
	"frid": preload("res://Packed/Fish/Frid.tscn"),
	"jackospungo": preload("res://Packed/Fish/JackoSpungo.tscn"),
	"melch": preload("res://Packed/Fish/Melch.tscn"),
	"nanba": preload("res://Packed/Fish/Nanba.tscn"),
	"redacted": preload("res://Packed/Fish/Redacted.tscn"),
	"smox": preload("res://Packed/Fish/Smox.tscn"),
	"treffi": preload("res://Packed/Fish/Treffi.tscn"),
	"tryndr": preload("res://Packed/Fish/Tyndr.tscn"),
	"ultraSpungle": preload("res://Packed/Fish/UltraSpungle.tscn")
}

onready var pLTestSprite = preload("res://Packed/TestSprite.tscn")
# Called when the node enters the scene tree for the first time.
func _ready():
	print("Print from Global")

func add_test_sprite(node, position, global = false):
	var spriteInst = pLTestSprite.instance()
	if global:
		spriteInst.global_position = position
	else:
		spriteInst.position = position
	node.add_child(spriteInst)
	return spriteInst
