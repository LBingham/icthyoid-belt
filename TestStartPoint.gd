extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	print("RayCast2D.is_colliding(): " + String($RayCast2D.is_colliding()))
	var spaceState = get_world_2d().direct_space_state
	var raycastResult = spaceState.intersect_ray($RayCast2D.global_position\
		, $RayCast2D.global_position + $RayCast2D.cast_to)
#	print("Script raycastResult: " + String(raycastResult))


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
