extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func initialize(shipPath, lockTargetPath):
	$PinJoint2D.set_node_a(shipPath)
	$PinJoint2D2.set_node_b(lockTargetPath)
	$PinJoint2D3.set_node_a(shipPath)
	$PinJoint2D3.set_node_b(lockTargetPath)
