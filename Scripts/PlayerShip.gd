extends RigidBody2D

const THRUST_POWER = 30
const ROTATE_POWER = 100
const ANGULAR_DAMPENING = 2
const ARM_ROTATION_RATE = .05
const SCAN_COMPLETION_RATE = .01
const SCAN_FLICKER_RATE_PER_DISTANCE = 500

enum ARM_STATES {STORED, DEPLOYING, RETRACTING, READY, ATTEMPING_LOCK, LOCKED}
enum ARM_TARGET_TYPES {NONE, UNPOPULATED, POPULATED}

onready var pLoadDockingStruct = preload("res://Packed/DockingStruct.tscn")
# Probe Arm Variables
var probeArmState = ARM_STATES.STORED
onready var myWorld = get_parent()
onready var armNode = $ProbeArm
onready var armSprite = $ProbeArm/AnimatedSprite
onready var armRetractTimer = $ProbeArm/ArmRetractTimer
onready var probeViewport = $ViewportContainer
onready var probeEnvironment = $ViewportContainer/Viewport/FluidicEnvironment
onready var animPlayer = $AnimationPlayer
onready var scannerSprite = $ScanIndicator/Sprite

var armCurrentTarget = null
var currentDockingStruct = null
var armCurrentTargetType = ARM_TARGET_TYPES.NONE

var scanCompletion = 0.0
var scanBlocked = false
var lastScannedResult = null

var inputThrustVectors = {"game_up": Vector2(0,-1), "game_down": Vector2(0,1), \
"game_left": Vector2(-1,0), "game_right": Vector2(1, 0)}

#var inputRotationVectors = {"game_rotateCCW": -1 * ROTATE_POWER, \
# "game_rotateCW": ROTATE_POWER}

# Called when the node enters the scene tree for the first time.
func _ready():
	probeEnvironment.connect("drone_deployed", self, "_on_drone_deployed")
	probeEnvironment.connect("drone_returned", self, "_on_drone_returned")

func _physics_process(delta):
	# Movement Thrusters
	applied_force = Vector2(0,0)
	if probeArmState != ARM_STATES.LOCKED:
		for action in inputThrustVectors:
			if Input.is_action_pressed(action):
				var thrustVec = (inputThrustVectors[action] \
				* THRUST_POWER).rotated(rotation)
				add_central_force(thrustVec)
	# Probe Arm
	process_probe_arm(delta)
	
	process_scan()

func process_probe_arm(delta):
	if Input.is_action_pressed("game_probeArmDeploy"):
		armRetractTimer.stop()
		if probeArmState == ARM_STATES.STORED:
			probe_arm_change_state(ARM_STATES.DEPLOYING)
		elif probeArmState == ARM_STATES.READY:
			if Input.is_action_just_pressed("game_probeArmDeploy"):
				probe_arm_change_state(ARM_STATES.ATTEMPING_LOCK)
			armNode.rotation += ARM_ROTATION_RATE
		elif probeArmState == ARM_STATES.LOCKED:
			if armCurrentTargetType == ARM_TARGET_TYPES.UNPOPULATED:
				if Input.is_action_just_pressed("game_probeArmDeploy"):
					undock_with_asteroid()
	elif probeArmState == ARM_STATES.READY:
		if armRetractTimer.is_stopped():
			armRetractTimer.start()

func process_scan():
	# Running Scan
	var scanTargetAcquired = false
	if animPlayer.current_animation != "scan complete":
		if Input.is_action_pressed("game_scan"):
			if lastScannedResult == null:
				if !scanBlocked:
					$ScanningLabel.visible = true
					animPlayer.play("scanning")
					scanCompletion += SCAN_COMPLETION_RATE
					if scanCompletion >= 1:
						scanCompletion = 0
						animPlayer.play("scan complete")
						scannerSprite.frame = 1
						scanTargetAcquired = true
				else:
					$ScanningLabel.visible = false
			else:
				unmark_last_scan_result()
		else:
			$ScanningLabel.visible = false
			animPlayer.stop()
			scanCompletion = 0
			scanBlocked = false
	else:
		scanBlocked = true
	
	if scanTargetAcquired:
		lastScannedResult = myWorld.get_closest_populated_asteroid(position)
		$ScanIndicator.visible = true
	
	# Pointing towards scan target
	if lastScannedResult != null:
		var angleToTarget = $ScanIndicator \
			.global_position.angle_to_point(lastScannedResult.global_position)
		var distanceToTarget = $ScanIndicator \
			.global_position.distance_to(lastScannedResult.global_position)
		scannerSprite.speed_scale = \
			clamp(SCAN_FLICKER_RATE_PER_DISTANCE/distanceToTarget, 0, 4)
		if scannerSprite.frame == 1:
			$ScanIndicator.rotation = angleToTarget

func probe_arm_change_state(state, dockingTarget = null):
	if state == ARM_STATES.STORED:
		armNode.visible = false
		probeArmState = ARM_STATES.STORED
		armSprite.play("stored")
	elif state == ARM_STATES.DEPLOYING:
		probeArmState = ARM_STATES.DEPLOYING
		armNode.visible = true
		armSprite.play("deploying")
	elif state == ARM_STATES.READY:
		if probeArmState == ARM_STATES.LOCKED:
			if currentDockingStruct != null:
				currentDockingStruct.queue_free()
				currentDockingStruct = null
		probeArmState = ARM_STATES.READY
		armSprite.play("ready")
	elif state == ARM_STATES.ATTEMPING_LOCK:
		probeArmState = ARM_STATES.ATTEMPING_LOCK
		armSprite.play("lockAttempt")
	elif state == ARM_STATES.LOCKED:
		probeArmState = ARM_STATES.LOCKED
		dock_with_asteroid(dockingTarget)
		armSprite.play("locked")
		armRetractTimer.stop()
	elif state == ARM_STATES.RETRACTING:
		probeArmState = ARM_STATES.RETRACTING
		armSprite.play("deploying", true)
	#print("Arm state: " + ARM_STATES.keys()[state])

func dock_with_asteroid(asteroidBody):
	if currentDockingStruct == null:
		currentDockingStruct = pLoadDockingStruct.instance()
		currentDockingStruct.initialize(self.get_path(), asteroidBody.get_path())
		add_child(currentDockingStruct)
		
		if myWorld.populatedAsteroids.has(asteroidBody):
			var asteroidPoly = asteroidBody.get_node("Polygon2D").polygon
			var relPosition = position - asteroidBody.position
			var shipArmAngle = armNode.rotation
			var shipInfo = {"relPosition": relPosition, "armAngle": shipArmAngle\
			, "shipScale": $Sprite.scale}
			armCurrentTargetType = ARM_TARGET_TYPES.POPULATED
			unmark_last_scan_result()
			var fishContent = asteroidBody.get_fish_contents()
			print("fishContent: " + String(fishContent))
	#		print("Ship position is: " + String(position))
	#		print("Asteroid position is: " + String(asteroidBody.position))
	#		print("Ship relative to asteroid is: " + String(position - asteroidBody.position))
			probeEnvironment.initialize(asteroidPoly, asteroidBody.rotation_degrees,fishContent \
			, shipInfo)
		else:
			armCurrentTargetType = ARM_TARGET_TYPES.UNPOPULATED

func undock_with_asteroid():
	probe_arm_change_state(ARM_STATES.READY)
	$ViewportContainer.visible = false
	probeEnvironment.deactivate()
	# Flag Asteroid
	if armCurrentTarget != null:
		var hasFlag = armCurrentTarget.get_node("Flag")
		if hasFlag == null:
			var flagInst = preload("res://Packed/Flag.tscn").instance()
			if armCurrentTargetType == ARM_TARGET_TYPES.POPULATED:
				flagInst.modulate = Color.green
			elif armCurrentTargetType == ARM_TARGET_TYPES.UNPOPULATED:
				flagInst.modulate = Color.red
			armCurrentTarget.add_child(flagInst)
			flagInst.global_position = $ProbeArm/ArmHitbox.global_position
			myWorld.mark_asteroid_as_docked(armCurrentTarget)
	
	armCurrentTargetType = ARM_TARGET_TYPES.NONE

func unmark_last_scan_result():
	lastScannedResult = null
	$ScanIndicator.visible = false

func _on_drone_deployed():
	$ViewportContainer.visible = true

func _on_AnimatedSprite_animation_finished():
	var animation = armSprite.animation
	if animation == "deploying":
		if probeArmState == ARM_STATES.DEPLOYING:
			probe_arm_change_state(ARM_STATES.READY)
		elif probeArmState == ARM_STATES.RETRACTING:
			probe_arm_change_state(ARM_STATES.STORED)
	elif animation == "lockAttempt":
		if armCurrentTarget != null:
			probe_arm_change_state(ARM_STATES.LOCKED, armCurrentTarget)
		else:
			probe_arm_change_state(ARM_STATES.READY)
		

func _on_ArmRetractTimer_timeout():
	probe_arm_change_state(ARM_STATES.RETRACTING)

func _on_ArmHitbox_body_entered(body):
	armCurrentTarget = body

func _on_ArmHitbox_body_exited(body):
	if armCurrentTarget == body:
		armCurrentTarget = null

func _on_drone_returned():
	undock_with_asteroid()
