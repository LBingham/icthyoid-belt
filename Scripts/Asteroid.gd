extends RigidBody2D

const POINT_DEVIATION = 15

export var fishRange = Vector2(3, 5)

var fishContents = null

# Called when the node enters the scene tree for the first time.
func initialize(texture):
	# Vary points of asteroid geometry
	randomize()
	var basePoly = $Polygon2D.polygon
	var newPoly = PoolVector2Array([])
	for vec in basePoly:
		vec.x += rand_range(-1 * POINT_DEVIATION, POINT_DEVIATION)
		vec.y += rand_range(-1 * POINT_DEVIATION, POINT_DEVIATION)
		newPoly.append(vec)
	$Polygon2D.polygon = newPoly
	$Polygon2D.texture = texture
	$CollisionPolygon2D.polygon = newPoly

func generate_fish_contents():
	if fishContents == null:
		fishContents = []
		var fishSpawnInfo = Global.FISH_SPAWN_INFO
		var fishSpawnThresholds = {}
		var totalWeight = 0
		# Determine total weight and order
		for fishIndex in fishSpawnInfo:
	#		print(fishIndex + " spawn threshold bottom at: " + String(totalWeight))
			fishSpawnThresholds[totalWeight] = fishIndex
			totalWeight += fishSpawnInfo[fishIndex]
	#	print("Final fish spawn thresholds: " + String(fishSpawnThresholds))
	#	print("Total weight: " + String(totalWeight))
		
		
		var fishCount = clamp(rand_range(fishRange.x, fishRange.y),1, fishRange.y)
	#	print("pickedSpawnWeight: " + String(pickedSpawnWeight))
		for thisInt in range(fishCount):
			var pickedSpawnWeight = rand_range(0, totalWeight)
			for thisWeight in fishSpawnThresholds:
				if pickedSpawnWeight < thisWeight:
					var spawnFish = fishSpawnThresholds[thisWeight]
					fishContents.append(spawnFish)
					break
		#			print("spawnFish: " + spawnFish)
	#	print("fishContents: " + String(fishContents))
	
func get_fish_contents():
	if fishContents == null:
		generate_fish_contents()
	return fishContents
