extends Node2D

const ARM_RAYCAST_SIZE = 500
const DRONE_OFFSET = 30
const RAYCAST_SIZE_INCREASE = 20
const POLY_MAGNIFICATION_FACTOR = 4

var basePolygon = null
var shipSprite = null
var armHeadSprite = null
var shipInfo = {}
var fishContent

var environmentMiddlePoint = null
var largestDistanceFromCenter = 0

signal drone_deployed
signal drone_returned

# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	#deactivate()

func initialize(polygon = null, polygonRotation = 0, thisFishContent = null, \
thisShipInfo = null, fishCount = 0):
	shipInfo = thisShipInfo
	basePolygon = polygon
	print("Fluidic Global: " + String(global_position))
	# Structure poly based off of polygon that was passed to function
	if basePolygon != null:
		var scaledUpPolygon = PoolVector2Array([])
		for vec in basePolygon:
			vec *= POLY_MAGNIFICATION_FACTOR
			scaledUpPolygon.append(vec)
		
		$Polygon2D.polygon = scaledUpPolygon
		$Polygon2D.rotation_degrees = polygonRotation
		
	fishContent = thisFishContent
	
	# Set Ship Position
	var scaledShipPosition
	if shipInfo != null:
		print("Passed ship position: " + String(shipInfo.relPosition))
		
		shipSprite = load("res://Packed/OutsideShipSprite.tscn").instance()
#		shipSprite.texture = load("res://Sprites/UFO_Piloted.png")
		shipSprite.scale = shipInfo.shipScale * POLY_MAGNIFICATION_FACTOR
		scaledShipPosition = shipInfo.relPosition * POLY_MAGNIFICATION_FACTOR
		shipSprite.position = scaledShipPosition
		#testSprite.scale = Vector2(1, 1)
		add_child(shipSprite)
		
		
		
	$CollisionPolygon2D.polygon = $Polygon2D.polygon
	$CollisionPolygon2D.rotation_degrees = $Polygon2D.rotation_degrees
	var polyPointSum = Vector2()
	var polyArray = $Polygon2D.polygon
	# Sum the points
	for thisVec in polyArray:
		polyPointSum += thisVec
	environmentMiddlePoint = polyPointSum / float(polyArray.size())
	#for thisFish in $Fish.get_children():
	#	thisFish.environmentMiddlePoint = environmentMiddlePoint
	
	# Calculate largest distance from center
	for thisVec in polyArray:
		var thisDistance = environmentMiddlePoint.distance_to(thisVec)
		if thisDistance > largestDistanceFromCenter:
			largestDistanceFromCenter = thisDistance
	print("Before raycast test")
	
	print("Polygon's build mode: " + String($CollisionPolygon2D.build_mode))
	
	#Inject probe
	$TimerDroneInject.start()

func inject_probe():
	if shipInfo != null:
		var startPoint = shipSprite.global_position
		var testDongle = preload("res://RayTestDongle.tscn").instance()
		var subNode = testDongle.get_node("Sprite")
		testDongle.global_position = startPoint
		testDongle.rotation = shipInfo.armAngle
		add_child(testDongle)
		
		var spaceState = get_world_2d().direct_space_state
		var raycastResult = spaceState.intersect_ray(testDongle.global_position, subNode.global_position)
		var raycastPosition = raycastResult.position
#		print("raycastResult: " + String(raycastResult))
		
		var droneInst = load("res://Packed/FishDrone.tscn").instance()
		var positionOffsetVector = \
		Vector2(DRONE_OFFSET, 0).rotated(shipInfo.armAngle)
		droneInst.position = raycastPosition + positionOffsetVector
		droneInst.rotation = shipInfo.armAngle
		add_child(droneInst)

		
		armHeadSprite = preload("res://Packed/ArmHeadSprite.tscn").instance()
		armHeadSprite.global_position = raycastPosition
		armHeadSprite.rotation = shipInfo.armAngle
		var armHeadArea = armHeadSprite.get_node("Area2D")
		armHeadArea.connect("body_entered", self, "_on_armhead_area_entered")
		add_child(armHeadSprite)
		
		emit_signal("drone_deployed")
		testDongle.queue_free()

func spawn_fish():
	print("fishContent in spawn_fish: " + String(fishContent))
	
	for fish in fishContent:
		var randPoint = get_random_point_in_environment( 20)
		print("randPoint generated: " + String(randPoint))
		var fishInst = Global.FISH_NAME_TO_PRELOAD[fish].instance()
		$Fish.add_child(fishInst)
		fishInst.global_position = randPoint

func _on_armhead_area_entered(body):
	print("Body entered")
	if body.is_in_group("fish drone"):
		emit_signal("drone_returned")

func deactivate():
	for fish in $Fish.get_children():
		fish.queue_free()
	var droneInst = get_node("FishDrone") 
	if droneInst != null:
		droneInst.queue_free()
	if shipSprite != null:
		shipSprite.queue_free()
	if armHeadSprite != null:
		armHeadSprite.queue_free()

func get_random_point_in_environment(shortening = 0):
	var randAngle = rand_range(0.0, 2.0 * PI)
	var rayLength = largestDistanceFromCenter + RAYCAST_SIZE_INCREASE
	var rayVec = Vector2(rayLength, 0).rotated(randAngle)
	var globalPositionOfMiddle = global_position + \
		 environmentMiddlePoint
	var rayEndVec = globalPositionOfMiddle + rayVec
	var spaceState = get_world_2d().direct_space_state
	var raycastResult = spaceState.intersect_ray(globalPositionOfMiddle, rayEndVec)
#	var middlePositionTestSprite = load("res://Packed/TestSprite.tscn").instance()
#	middlePositionTestSprite.global_position = globalPositionOfMiddle
#	var raycastResultSprite = load("res://Packed/TestSprite.tscn").instance()
#	raycastResultSprite.global_position = raycastResult.position
#	myEnvironment.add_child(raycastResultSprite)
	var rayEndEdgeVec
	if raycastResult.size() == 0:
		printerr("Raycast didn't succeed. Creating debug sprites")
		var startSprite = Global.add_test_sprite(self, globalPositionOfMiddle, true)
		startSprite.modulate = Color.red
		var endSprite = Global.add_test_sprite(self, rayEndVec, true)
		endSprite.modulate = Color.green
		rayEndEdgeVec = Vector2(0, 0)
	rayEndEdgeVec = raycastResult.position
	var distToEdge = globalPositionOfMiddle.distance_to(rayEndEdgeVec)
	var targetPosition = globalPositionOfMiddle + \
		Vector2(rand_range(0, distToEdge - shortening), 0).rotated(randAngle)
	return targetPosition

func get_middle_point():
	return environmentMiddlePoint

func get_largest_distance_from_center():
	return largestDistanceFromCenter


func _on_TimerDroneInject_timeout():
	spawn_fish()
	inject_probe()
