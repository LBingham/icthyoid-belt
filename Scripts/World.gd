extends Node2D

const ASTRD_AXISCOUNT = 50
const ASTRD_SPAWN_CHANCE = .75
const STRD_ENVIRONMENT_CHANCE = 1#.03
const ASTRD_SPACE_DISTANCE = 250
const ASTRD_TYPES = 4
const ASTRD_SPEED_MAX = 30
const SRD_SPEED_MIN = 15

onready var pLoadAsteroidTexture = preload("res://Textures/Asteroid Texture.tres")

var populatedAsteroids = []
var populatedUndockedAsteroids = []

# Called when the node enters the scene tree for the first time.
func _ready():
	generate_asteroid_field()

func generate_asteroid_field():
	# Associate each asteroid type with its respective spawn threshold
	# The pickRoll has to be lower than the spawn threshold, but higher than other
	# spawn thresholds, to spawn
	var astPLoadChances = {}
	var astSpawnChanceRange = 1.0/float(ASTRD_TYPES)
	for thisInt in range(ASTRD_TYPES):
		var thisString = "res://Packed/Ast" + String(thisInt + 1) + ".tscn"
		var thisPLoad = load(thisString)
		astPLoadChances[float(astSpawnChanceRange * thisInt + astSpawnChanceRange)] = thisPLoad
	# On a grid that is ASTRD_AXISCOUNT by ASTRD_AXISCOUNT big, give a chance
	# to spawn an asteroid
#	print("astPLoadChances: " + String(astPLoadChances))
	var thisSpot
	for xCount in range(-.5 * ASTRD_AXISCOUNT, .5 * ASTRD_AXISCOUNT):
		for yCount in range(-.5 * ASTRD_AXISCOUNT, .5 * ASTRD_AXISCOUNT):
			var asteroidRoll = rand_range(0, 1)
			if asteroidRoll < ASTRD_SPAWN_CHANCE:
				thisSpot = Vector2(ASTRD_SPACE_DISTANCE *  xCount, \
				ASTRD_SPACE_DISTANCE * yCount)
				var pickRoll = rand_range(0, 1)
				var thisInst
				for thisFloat in astPLoadChances:
					if pickRoll < thisFloat:
						thisInst = astPLoadChances[float(thisFloat)].instance()
						break
				$Asteroids.add_child(thisInst)
				thisInst.initialize(pLoadAsteroidTexture)
				thisInst.position = thisSpot
				thisInst.rotation = rand_range(0, 2* PI)
				thisInst.linear_velocity = Vector2(rand_range(SRD_SPEED_MIN, \
				ASTRD_SPEED_MAX), 0).rotated(rand_range(0, 2 * PI))
				
				# Asteroid Environment roll
				pickRoll = rand_range(0, 1)
				if pickRoll < STRD_ENVIRONMENT_CHANCE:
					populatedAsteroids.append(thisInst)
	# Set lists of populated asteroids and unscanned populated asteroids to the 
	# same value
	populatedUndockedAsteroids = populatedAsteroids

func get_closest_populated_asteroid(thisPosition):
#	print("Passed position is: " + String(thisPosition))
	var pos2asteroid = {}
	for asteroid in populatedUndockedAsteroids:
		if asteroid != null:
			pos2asteroid[asteroid.position] = asteroid
	#print("pos2asteroid: " + String(pos2asteroid))
	
	var nearestAsteroid = null
	var shortestDistance = null
	for asterPos in pos2asteroid:
		var positionDifference = thisPosition.distance_to(asterPos)
		if nearestAsteroid == null || positionDifference < shortestDistance:
			nearestAsteroid = pos2asteroid[asterPos]
			shortestDistance = positionDifference
	
	#nearestAsteroid.modulate = Color.red
	return nearestAsteroid
#	print("nearestAsteroid: " + String(nearestAsteroid.name))
#	print("shortestDistance: " + String(shortestDistance))
	

func mark_asteroid_as_docked(asteroid):
	if populatedAsteroids.has(asteroid):
		var astIndex = populatedUndockedAsteroids.find(asteroid)
		populatedUndockedAsteroids.remove(astIndex)
		print("Removed: " + asteroid.name)
	else:
		print("Asteroid not in list of populated asteroids")
