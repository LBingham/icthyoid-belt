extends Area2D


func _on_SpawnAreaClear_body_entered(body):
	if body.is_in_group("asteroid"):
		body.queue_free()


func _on_Timer_timeout():
	queue_free()
