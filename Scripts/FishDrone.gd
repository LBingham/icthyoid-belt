extends KinematicBody2D

const SWIM_ACCEL = 4
const SWIM_MAX_SPEED = 200
const TURN_RATE_PER_SPEED = .015
const BASE_TURN_RATE_SPEED_THRESHOLD = 100
const BASE_TURN_RATE = 1.75
const WATER_DEACCEL = 1
const TAIL_ANGULAR_SPEED = 6
const TAIL_ANGLE_THRESHOLD = 20
const HEAD_ANGULAR_SPEED = 3
const HEAD_ANGLE_THRESHOLD = 10

onready var headSprite = $HeadSprite
onready var tailSprite = $HeadSprite/TailSprite

var speed = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _physics_process(delta):
	var tailAngle = tailSprite.rotation_degrees
	var headAngle = headSprite.rotation_degrees
	var addSpeed = false
	var turnSign = 0
	if Input.is_action_pressed("game_left"):
		# Rotate Tail
		if tailAngle < TAIL_ANGLE_THRESHOLD:
			tailSprite.rotation_degrees += TAIL_ANGULAR_SPEED
			if tailAngle > TAIL_ANGLE_THRESHOLD:
				tailSprite.rotation_degrees = TAIL_ANGLE_THRESHOLD
			addSpeed = true
		turnSign = -1
		# Rotate Head
		if headAngle < HEAD_ANGLE_THRESHOLD:
			headSprite.rotation_degrees += HEAD_ANGULAR_SPEED
			if headAngle > HEAD_ANGLE_THRESHOLD:
				headSprite.rotation_degrees = HEAD_ANGLE_THRESHOLD
			
	elif Input.is_action_pressed("game_right"):
		if tailAngle > -TAIL_ANGLE_THRESHOLD:
			tailSprite.rotation_degrees -= TAIL_ANGULAR_SPEED
			if tailAngle < -TAIL_ANGLE_THRESHOLD:
				tailSprite.rotation_degrees = -TAIL_ANGLE_THRESHOLD
			addSpeed = true
		turnSign = 1
		# Rotate Head
		if headAngle > -HEAD_ANGLE_THRESHOLD:
			headSprite.rotation_degrees -= HEAD_ANGULAR_SPEED
			if headAngle < -HEAD_ANGLE_THRESHOLD:
				headSprite.rotation_degrees = HEAD_ANGLE_THRESHOLD
	else:
		var rotationSign = sign(tailAngle)
		tailSprite.rotation_degrees -= rotationSign * TAIL_ANGULAR_SPEED
		headSprite.rotation_degrees -= rotationSign * HEAD_ANGULAR_SPEED
		
	if addSpeed && speed < SWIM_MAX_SPEED:
		speed += SWIM_ACCEL
		if speed > SWIM_MAX_SPEED:
			speed = SWIM_MAX_SPEED
	
	if speed > 0:
		speed -= WATER_DEACCEL
		if speed < 0:
			speed = 0
	
	var velocity = Vector2(speed, 0).rotated(rotation)
	var baseTurnSpeedAdd = 0
	if turnSign != 0 && speed < BASE_TURN_RATE_SPEED_THRESHOLD:
		baseTurnSpeedAdd += BASE_TURN_RATE * turnSign
	rotation_degrees += speed * turnSign * TURN_RATE_PER_SPEED + baseTurnSpeedAdd
	move_and_slide(velocity)
