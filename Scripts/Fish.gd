extends KinematicBody2D

const WATER_DEACCELERATION = 1.25
#const RAYCAST_SIZE_INCREASE = 20
const SWIM_THRUST_VEC = Vector2(90, 0)
const SWIM_VEC_VARIATION = PI/10
const SWIM_TARGET_DETECTION_DISTANCE = 50
const WIGGLE_TIME_OFFSET = .75
const BASE_WIGGLE_TIME = 1.5
const WIGGLE_ANIM_ANGLE = 5
const BASE_SCALE = Vector2(2, 2)


onready var myEnvironment = get_parent().get_parent()
#onready var myEnvironmentFluidPolygon = get_node("../Polygon2D")

onready var pLoadAreaTester = preload("res://Packed/FishSwimTargetTestingArea.tscn")

var velocity = Vector2(0,0)

var swimTarget = null

func _ready():
	scale = BASE_SCALE
	randomize()

func _physics_process(delta):
	if swimTarget != null:
		if $TimerWiggle.is_stopped():
			$TimerWiggle.start()
		
		#TESTING CODE. REMOVE
		if global_position.distance_to(swimTarget) < SWIM_TARGET_DETECTION_DISTANCE:
			select_new_swim_target()
		
	if velocity.length() > 10:
		if abs(velocity.angle_to(Vector2(-1, 0))) < PI/2:
			$Sprite.scale.y = -1
		else:
			$Sprite.scale.y = 1
	
	move_and_slide(velocity)
	if velocity.length() > WATER_DEACCELERATION:
		var deaccelVec = velocity.normalized() * -WATER_DEACCELERATION
		velocity += deaccelVec
	else:
		velocity = Vector2(0, 0)
	
	
func select_new_swim_target():
	var targetDecided = false
	var targetPosition
	var repeatCounts = 0
	while !targetDecided:
		targetPosition = myEnvironment.get_random_point_in_environment()
		if targetPosition != swimTarget:
			targetDecided = true
		else:
#			print("Repeat target detected")
			randomize()
			if repeatCounts >= 10:
				print("Repeat limit reached")
				targetPosition = Vector2(100,100)
				break
			else:
				repeatCounts += 1
				
	swimTarget = targetPosition
#	print("New swimTarget: " + String(swimTarget))
#	var targetResultSprite = load("res://Packed/TestSprite.tscn").instance()
#	targetResultSprite.scale = Vector2(.5, .5)
#	targetResultSprite.global_position = targetPosition
#	myEnvironment.add_child(targetResultSprite)

func _on_TimerNextSwimTarget_timeout():
	select_new_swim_target()

func _on_TimerWiggle_timeout():
	if global_position.distance_to(swimTarget) < SWIM_TARGET_DETECTION_DISTANCE:
		select_new_swim_target()
	else:
		var angleTowardsTarget = swimTarget.angle_to_point(global_position)
		var swimAngle = angleTowardsTarget + \
			rand_range(-1 * SWIM_VEC_VARIATION, SWIM_VEC_VARIATION)
		align_wiggle_animation(swimAngle)
		$AnimationPlayer.play("wiggle")
		#$Sprite.rotation = angleTowardsTarget
		
		var swimVec = SWIM_THRUST_VEC.rotated(swimAngle)
#		print("swimVec is: " + String(swimVec))
		velocity = swimVec
	
	$TimerWiggle.wait_time = BASE_WIGGLE_TIME + \
	rand_range(-WIGGLE_TIME_OFFSET, WIGGLE_TIME_OFFSET)

func align_wiggle_animation(angle):
	var wiggleAnim = $AnimationPlayer.get_animation("wiggle")
	#var trackID = wiggleAnim.find_track("Sprite:rotation_degrees")
	var keyID = wiggleAnim.track_find_key(0, .1)
#	print("keyID is: " + String(keyID))
#	print("sent angle is: " + String(angle))
	wiggleAnim.track_set_key_value(0, 0,rad2deg(angle) + WIGGLE_ANIM_ANGLE)
	wiggleAnim.track_set_key_value(0, 1,rad2deg(angle) - WIGGLE_ANIM_ANGLE)
	wiggleAnim.track_set_key_value(0, 2,rad2deg(angle) + WIGGLE_ANIM_ANGLE)
	wiggleAnim.track_set_key_value(0, 3,rad2deg(angle))

